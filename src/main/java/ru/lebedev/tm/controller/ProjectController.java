package ru.lebedev.tm.controller;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.service.ProjectService;
import ru.lebedev.tm.service.UserService;

import java.util.List;
import java.util.Objects;

import static ru.lebedev.tm.constant.UserAlert.ACCESS_LEVEL_INSUFFICIENT;
import static ru.lebedev.tm.constant.UserAlert.NOT_LOGGED;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;
    private final UserService userService;

    public ProjectController(ProjectService projectService, UserService userService) {
        this.projectService = projectService;
        this.userService = userService;
    }

    public int createProject() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        List<Project> existProjectName = projectService.findByName(name);
        if(!existProjectName.isEmpty()) {
            System.out.println("A PROJECT NAMED " + "'" + name + "' ALREADY EXIST");
            System.out.println("[FAIL]");
            return -2;
        }
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.create(name, description, userService.getAuthUserId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        try {
            System.out.println("[UPDATE PROJECT]");
            System.out.println("ENTER, PROJECT INDEX:");
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Project project = projectService.findByIndex(index);
            if (project == null) {
                System.out.println("[FAIL]");
                return 0;
            }
            final Long authUser = userService.getAuthUserId();
            if (userService.authUserIsAdmin(authUser) || authUser.equals(project.getUserId())) {
                System.out.println("PLEASE, ENTER PROJECT NAME:");
                final String name = scanner.nextLine();
                System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
                final String description = scanner.nextLine();
                projectService.update(project.getId(), name, description);
                System.out.println("[OK]");
                return 0;
            } else {
                System.out.println(ACCESS_LEVEL_INSUFFICIENT);
                return -1;
            }
        } catch (NumberFormatException e) {
            throw new ProjectNotFoundException("The entered data is not a number - " + e.getMessage());
        }
    }

    public int removeProjectByName() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final List<Project> project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeProjectById() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        try {
            System.out.println("[REMOVE PROJECT BY ID]");
            System.out.println("PLEASE, ENTER PROJECT ID:");
            final long id = Long.parseLong(scanner.nextLine());
            final Project project = projectService.removeById(id);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
            return 0;
        } catch (NumberFormatException e) {
            throw new ProjectNotFoundException("The entered data is not a number - " + e.getMessage());
        }
    }

    public int removeProjectByIndex() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        try {
            System.out.println("[REMOVE PROJECT BY INDEX]");
            System.out.println("PLEASE, ENTER PROJECT INDEX:");
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Project project = projectService.removeByIndex(index);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
            return 0;
        } catch (NumberFormatException e) {
                throw new ProjectNotFoundException("The entered data is not a number - " + e.getMessage());
            }


    }

    public void viewProject(final Project project) {
        if (Objects.isNull(project)) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("USER ID: " + project.getUserId());
        if (!Objects.isNull(project.getUserId())) {
            final User user = userService.findById(project.getUserId());
            if(!Objects.isNull(user))
                System.out.println("USER LOGIN: " + user.getLogin());
        }
        System.out.println("[OK]");

    }

    public int viewProjectByIndex() throws ProjectNotFoundException {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        try {
            System.out.println("ENTER, PROJECT INDEX:");
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Project project = projectService.findByIndex(index);
            final Long authUser = userService.getAuthUserId();
            if (userService.authUserIsAdmin(authUser) || authUser.equals(project.getUserId())) {
                viewProject(project);
                return 0;
            } else {
                System.out.println(ACCESS_LEVEL_INSUFFICIENT);
                return -1;
            }
        } catch (NumberFormatException e) {
            throw new ProjectNotFoundException("The entered data is not a number - " + e.getMessage());
        }
    }

    public int clearProject() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        if (!userService.authUserIsAdmin(userService.getAuthUserId())) {
            System.out.println(ACCESS_LEVEL_INSUFFICIENT);
            return -1;
        }
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject() {
        if (userService.noAuthUser()) {
            System.out.println(NOT_LOGGED);
            return -1;
        }
        System.out.println("[LIST PROJECT]");
        final Long authUser = userService.getAuthUserId();
        if (userService.authUserIsAdmin(authUser)) {
            viewProjects(projectService.findAllOrderByName());
        } else {
            viewProjects(projectService.findAllOrderByName(authUser));
        }
        System.out.println("[OK]");
        return 0;

    }

    public void viewProjects(final List<Project> projects){
        if (userService.noAuthUser()){
            System.out.println(NOT_LOGGED);
            return;
        }
        if(Objects.isNull(projects) || projects.isEmpty()) return;
        int index = 1;
        for (final Project project: projects ){
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + "| USER ID: " + project.getUserId());
            index++;
        }
    }

}
