package ru.lebedev.tm.service;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.repository.ProjectRepository;
import ru.lebedev.tm.repository.TaskRepository;

import java.util.Objects;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;


    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) throws TaskNotFoundException {

        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(Objects.isNull(task) ) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) throws TaskNotFoundException, ProjectNotFoundException {

        final Project project = projectRepository.findById(projectId);
        if(Objects.isNull(project) ) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

}
