package ru.lebedev.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.lebedev.tm.constant.UserAlert;
import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.enumerated.RoleType;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.repository.ProjectRepository;
import ru.lebedev.tm.repository.TaskRepository;
import ru.lebedev.tm.repository.UserRepository;
import ru.lebedev.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

public class UserService {

    private Long authUserId = 0L;
    private final UserRepository userRepository;

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;


    public UserService(UserRepository inUserRepository, ProjectRepository inProjectRepository, TaskRepository inTaskRepository) {
        this.userRepository = inUserRepository;
        this.projectRepository = inProjectRepository;
        this.taskRepository = inTaskRepository;
    }

    public User create(
            final String login, final String password
    ){
        if (login == null || login.isEmpty()) return null;
        if (StringUtils.isEmpty(password)) return null;
        String hPassword = HashUtil.hashStringMD5(password);
        if(StringUtils.isEmpty(hPassword)) return null;
        return userRepository.create(login, hPassword);
    }

    public User create(
            final String login, final String password, RoleType roleType,
            final String firstName, final String middleName, final String secondName
    ) {
        if (
                StringUtils.isEmpty(login)||StringUtils.isEmpty(password)||
                StringUtils.isEmpty(firstName)||StringUtils.isEmpty(middleName)||
                StringUtils.isEmpty(secondName)
        ) return null;
        if (Objects.isNull(roleType)) return null;
        String hPassword = HashUtil.hashStringMD5(password);
        if(StringUtils.isEmpty(hPassword)) return null;

        return userRepository.create(login, hPassword, roleType, firstName, middleName, secondName);
    }

    public User update(
            final Long id, final String firstName, final String middleName, final String secondName,
            final String login, RoleType roleType
    ){
        if(id == null) return null;
        if (
                StringUtils.isEmpty(login)||StringUtils.isEmpty(firstName)||
                StringUtils.isEmpty(middleName)||StringUtils.isEmpty(secondName)
        ) return null;
        if (Objects.isNull(roleType)) return null;
        return userRepository.update(id, firstName, middleName, secondName, login, roleType);
    }

    public User update(final Long id, final String password) {
        if (id == null) return null;
        if (StringUtils.isEmpty(password)) return null;
        String hPassword = HashUtil.hashStringMD5(password);
        if (StringUtils.isEmpty(hPassword)) return null;
        return userRepository.update(id, hPassword);
    }

    public User update(final Long id, RoleType roleType) {
        if (id == null) return null;
        if (roleType.toString() == null) return null;
        return userRepository.update(id, roleType);
    }



      public void clear() {
        userRepository.clear();
    }

    public User findByIndex(int index) {
        if (index < 0 || index > userRepository.numberOfUsers() - 1) return null;
        return userRepository.findByIndex(index);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByLogin(String login) {
        if(login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User removeByIndex(int index) {
        if (index < 0 || index > userRepository.numberOfUsers() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeById(Long id) {
        if(id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public boolean passwordVerification(User authUser, String userPassword) {
        if (Objects.isNull(authUser)) return false;
        String password = HashUtil.hashStringMD5(userPassword);
        if(StringUtils.isEmpty(password)) return false;
        if(password.equals(authUser.getPassword())) return true;
        else return false;

    }

    public boolean noAuthUser(){
        if (this.getAuthUserId() == 0)
            return true;
        else return false;
    }

    public RoleType roleTypeAuthUser(final Long authUserId) {
        if (authUserId == null || authUserId == 0) return null;
        final User authUser = this.findById(authUserId);
        if (Objects.isNull(authUser)) return null;
        final RoleType roleType = authUser.getUserRole();
        return roleType;
    }

    public boolean authUserIsAdmin(final Long authUserId) {
        if (authUserId == null || authUserId == 0) return false;
        final RoleType roleType = this.roleTypeAuthUser(authUserId);
        if (roleType.equals(RoleType.ADMIN)) return true;
        else return false;
    }

    public Task addTaskToUser(final Long taskId, final Long userId) throws TaskNotFoundException {
        if(Objects.isNull(taskId) || Objects.isNull(userId)) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        final User user = userRepository.findById(userId);
        if(user == null) return null;
        task.setUserId(userId);
        return task;
    }
    public Task removeUserFromTask(final Long taskId, final Long userId){
        if(Objects.isNull(taskId) || Objects.isNull(userId)) return null;
        final Task task = taskRepository.findByUserIdAndId(userId, taskId);
        if(task == null) return null;
        task.setUserId(null);
        return task;
    }

    public Project addProjectToUser(final Long projectId, final Long userId) throws ProjectNotFoundException {
        if(Objects.isNull(projectId) || Objects.isNull(userId)) return null;
        final Project project = projectRepository.findById(projectId);
        if(Objects.isNull(project)) return null;
        final User user = userRepository.findById(userId);
        if(Objects.isNull(user)) return null;
        project.setUserId(userId);
        return project;
    }

    public Project removeUserFromProject(final Long projectId, final Long userId){
        if(Objects.isNull(projectId) || Objects.isNull(userId)) return null;
        final Project project = projectRepository.findByUserIdAndId(userId, projectId);
        if(project == null) return null;
        project.setUserId(null);
        return project;
    }


    public Long getAuthUserId() {
        return authUserId;
    }

    public void setAuthUserId(Long authUserId) {
        this.authUserId = authUserId;
    }
}
