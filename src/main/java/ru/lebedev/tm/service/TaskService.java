package ru.lebedev.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.exception.ProjectNotFoundException;
import ru.lebedev.tm.exception.TaskNotFoundException;
import ru.lebedev.tm.repository.TaskRepository;

import java.util.List;
import java.util.Objects;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if(StringUtils.isEmpty(name)) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description) || userId == null) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(Long id, String name, String description) throws TaskNotFoundException {
        if(id == null | StringUtils.isEmpty(name) | StringUtils.isEmpty(description)) {
            throw new TaskNotFoundException("The main parameters of the task cannot be empty. Id = '" + id
                                             + "'; Name = '" + name + "'; Descr. = '" + description + "'.");
        }
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(int index) throws TaskNotFoundException {
        if (index < 0 || index > taskRepository.numberOfTasks() - 1) {
            throw new TaskNotFoundException("Task Index = " + (index + 1) + " does not exist");
        }
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) throws TaskNotFoundException {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.findByName(name);
    }
    public List<Task> findByNameIndex(final String name) throws TaskNotFoundException {
        if (StringUtils.isEmpty(name)) {
            throw new TaskNotFoundException("A task cannot have an empty name.");
        }
        return taskRepository.findByNameIndex(name);
    }

    public List<Task> findByNameIndex(final String name, final Long id) throws TaskNotFoundException{
        if (StringUtils.isEmpty(name)) {
            throw new TaskNotFoundException("A task cannot have an empty name.");
        }
        if (id == null) {
            throw new TaskNotFoundException("User must have ID");
        }
        return taskRepository.findByNameIndex(name, id);
    }


    public Task removeById(Long id) throws TaskNotFoundException {
        if(id == null) throw new TaskNotFoundException("An empty task Id is not valid!");
        return taskRepository.removeById(id);
    }

    public List<Task> removeByName(String name) throws TaskNotFoundException {
        if(StringUtils.isEmpty(name)) return null;
        return taskRepository.removeByName(name);
    }

    public Task removeByIndex(int index) throws TaskNotFoundException {
        if (index < 0 || index > taskRepository.numberOfTasks() - 1) {
            throw new TaskNotFoundException("Task with index = " + index + " does not exist");
        };
        return taskRepository.removeByIndex(index);
    }

    public Task findById(Long id) throws TaskNotFoundException {
        if(id == null) return null;
        return taskRepository.findById(id);
    }

     public List<Task> findAllOrderByName() {
        return  taskRepository.findAllOrderByName();
    }

    public List<Task> findAllOrderByName(final Long userId) {
        return  taskRepository.findAllOrderByName(userId);
    }

    public List<Task> findAllByProjectId(Long projectId) throws TaskNotFoundException {
        if(projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(Long projectId, Long id) throws TaskNotFoundException {
        if(projectId == null  || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }
}